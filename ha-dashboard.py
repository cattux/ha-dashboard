#!/usr/bin/python3

import paho.mqtt.client as mqtt
import time
import os
import config

def on_message(client, userdata, message):
    """ 
    Commands accepted:
        Topic: ha/ha-dashboard/setbrightness
            given string is used as the volume percentage
            Ex: "255" for a message would set the brightness to 100%
    """
    messy = str(message.payload.decode("utf-8"))
    if message.topic == "ha/ha-dashboard/setbrightness":
        try:
            messy = int(messy)
            if 0 <= messy <= 255:
                command = "sudo sh -c 'echo '{}' > /sys/class/backlight/rpi_backlight/brightness' &".format(messy)
                print(command)
                os.system(command)
                client.publish("ha/ha-dashboard/brightness","{}".format(messy))
            else:
                print("You must enter an integer between 0 and 255")
        except:
            print("You must give an integer as a message")

def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True
    else:
        print("Bad connection; Returned code: ",rc)

if  __name__ == "__main__":

    print("Starting HA-Dashboard")

    broker_address = config.broker_address
    client_name = config.client_name
    client = mqtt.Client(client_name)
    client.connected_flag=False
    client.on_connect=on_connect
    client.on_message = on_message
    client.loop_start()
    print("Connecting to broker {}".format(broker_address))

    client.connect(broker_address)
    while not client.connected_flag:
        print("Waiting to connect")
    print("Now connected to broker {}".format(broker_address))

    subscription = "ha/#"
    client.subscribe(subscription)
    print("Now subscripted to {}".format(subscription))

    while True:
        """ Infinite loop to keep looking for pin interupts or MQTT messages"""
        pass

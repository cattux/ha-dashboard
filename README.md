# ha-dashboard

A setup guide and accompanying scripts to run a Raspberry Pi as a Home Assistant Dashboard. To allow Home Assistant to interact with the dashboard, and vise versa, the two will communicate over MQTT.

Fresh install of Rasbian (Lite if it'll be headless)

set up Kiosk mode

Clone repo to /home/pi/

git clone https://gitlab.com/cattux/ha-dashboard.git


Install mqtt

pip3 install paho-mqtt


Update config.py file with your MQTT broker info (broker_address and client_name)


Run ha-dashboard

python3 ha-dashboard.py


once everything is working properly, turn script into a service

sudo systemctl edit --force --full ha-dashboard.service


[Unit]

Description=HA-Dashboard

Wants=network-online.target

After=network-online.target

[Service]

Type=idle

User=pi

WorkingDirectory=/home/pi/ha-dashboard

ExecStart=/home/pi/ha-dashboard/ha-dashboard.py

Restart=always

RestartSec=30

[Install]

WantedBy=multi-user.target



Then enable the service:

sudo systemctl enable ha-dashboard.service

Then either reboot the Pi or start the service:

sudo systemctl start ha-dashboard.service